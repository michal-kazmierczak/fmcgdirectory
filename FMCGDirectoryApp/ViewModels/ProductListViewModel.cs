﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel;
using System.Collections.ObjectModel;
using BLC;
using Interfaces;
using System.Windows.Input;

namespace FMCGDirectoryApp.ViewModels
{
    class ProductListViewModel: ViewModelBase
    {
        public ObservableCollection<ProductViewModel> Products
        {
            get => _products;
        }
        public ObservableCollection<ISupermarket> Supermarkets
        {
            get => _supermarkets;
        }
        public ProductViewModel SelectedProduct
        {
            set
            {
                _selectedProduct = value;
                RaisePropertyChanged("SelectedProduct");
            }
            get
            {
                return _selectedProduct;
            }
        }
        public ICommand AddNewProductCommand
        {
            get { return addNewProductCommand; }
        }
        public ICommand RemoveSelectedProductCommand
        {
            get { return removeSelectedProductCommand; }
        }
        public String SearchTerm
        {
            set
            {
                _searchTerm = value;
                foreach(var p in _products)
                {
                    if(p.Name.StartsWith(value, StringComparison.InvariantCultureIgnoreCase) ||
                        p.Category.ToString().StartsWith(value, StringComparison.InvariantCultureIgnoreCase) ||
                        p.Supermarket.Name.StartsWith(value, StringComparison.InvariantCultureIgnoreCase) ||
                        p.PriceDiscount.ToString().StartsWith(value) ||
                        p.PriceRegular.ToString().StartsWith(value))
                    {
                        p.IsVisible = true;
                    }
                    else
                    {
                        p.IsVisible = false;
                    }
                } 

            }
            get
            {
                return _searchTerm;
            }
        }
        private ObservableCollection<ProductViewModel> _products;
        private ObservableCollection<ISupermarket> _supermarkets;
        private ProductViewModel _selectedProduct;
        private String _searchTerm = "Szukaj...";
        private RepositoryDAO repositoryDAO;
        private RelayCommand addNewProductCommand;
        private RelayCommand removeSelectedProductCommand;
        public ProductListViewModel()
        {
            // Load database name from the project's properties
            string datastoreName = Properties.Settings.Default.datastoreName;
            repositoryDAO = new RepositoryDAO(datastoreName);

            // fetch products
            _products = new ObservableCollection<ProductViewModel>();
            foreach (var p in repositoryDAO.Products)
            {
                _products.Add(new ProductViewModel(p));
            }

            // fetch supermarkets
            _supermarkets = new ObservableCollection<ISupermarket>(repositoryDAO.Supermarkets);
            Console.WriteLine(_supermarkets.Count);

            addNewProductCommand = new RelayCommand(param => this.AddProductAction());
            removeSelectedProductCommand = new RelayCommand(param => this.RemoveSelectedProductAction());
        }

        private void AddProductAction()
        {
            var newProduct = new ProductViewModel(repositoryDAO.CreateProduct());
            _products.Add(newProduct);
            SelectedProduct = newProduct;
        }

        private void RemoveSelectedProductAction()
        {
            repositoryDAO.RemoveProduct(SelectedProduct.Product);
            _products.Remove(SelectedProduct);
        }
    }
}
