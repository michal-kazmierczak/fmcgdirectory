﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using System.Windows.Input;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace FMCGDirectoryApp.ViewModels
{
    class ProductViewModel : ViewModelBase//, INotifyDataErrorInfo
    {

        private IProduct _product;
        private static readonly Regex _floatRegex = new Regex("[^0-9.-]+");
        private bool _isVisible = true;
        public ProductViewModel(IProduct product)
        {
            _product = product;
        }
        public IProduct Product
        {
            get { return _product; }
        }
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            { 
                _isVisible = value; 
                RaisePropertyChanged("IsVisible");
            }
        }
        public Guid ID
        {
            get { return _product.ID; }
            set { }
        }

        [Required]
        [StringLength(32, MinimumLength = 3)]
        public string Name
        {
            get { return _product.Name; }
            set
            {
                _product.Name = value;
                RaisePropertyChanged("Name");
                this.Validate();
            }
        }

        [Required]
        public Category? Category
        {
            get { return _product.Category; }
            set
            {
                _product.Category = value;
                RaisePropertyChanged("Category");
                this.Validate();
            }
        }

        [Required]
        public ISupermarket Supermarket
        {
            get { return _product.Supermarket; }
            set
            {
                _product.Supermarket = value;
                RaisePropertyChanged("Supermarket");
                this.Validate();
            }
        }
        [Required]
        public double PriceRegular
        {
            get { return _product.PriceRegular; }
            set
            {
                _product.PriceRegular = value;
                RaisePropertyChanged("PriceRegular");
                RaisePropertyChanged("DiscountAsPercentage");
                this.Validate();
            }
        }
        [Required]
        public double PriceDiscount
        {
            get { return _product.PriceDiscount; }
            set
            {
                _product.PriceDiscount = value;
                RaisePropertyChanged("PriceDiscount");
                this.Validate();
            }
        }

        public int DiscountAsPercentage
        {
            get { return _product.DiscountAsPercentage; }
        }

        private readonly Dictionary<string, ICollection<string>> _validationErrors = new Dictionary<string, ICollection<string>>();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void RaiseErrorChanged(string property)
        {
            if (ErrorsChanged != null)
            {
                ErrorsChanged(this, new DataErrorsChangedEventArgs(property));
                RaisePropertyChanged("HasErrors");
            }
        }
        public void Validate()
        {

            var validationContext = new ValidationContext(this, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(this, validationContext, validationResults, true);

            foreach (var kv in _validationErrors.ToList())
            {
                if (validationResults.All(r => r.MemberNames.All(m => m != kv.Key)))
                {
                    _validationErrors.Remove(kv.Key);
                    RaiseErrorChanged(kv.Key);
                }
            }

            var q = from r in validationResults
                    from m in r.MemberNames
                    group r by m into g
                    select g;

            foreach (var prop in q)
            {
                var messages = prop.Select(r => r.ErrorMessage).ToList();

                if (_validationErrors.ContainsKey(prop.Key))
                {
                    _validationErrors.Remove(prop.Key);
                }
                _validationErrors.Add(prop.Key, messages);
                RaiseErrorChanged(prop.Key);
            }

        }

        private static bool IsTextAllowedForFloat(string text)
        {
            return !_floatRegex.IsMatch(text);
        }
    }
}
