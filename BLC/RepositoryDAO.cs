﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using System.Reflection;

namespace BLC
{
    public class RepositoryDAO
    {
        public List<IProduct> Products
        {
            get => _products;
        }
        public List<ISupermarket> Supermarkets
        {
            get => _supermarkets;
        }
        private List<IProduct> _products;
        private List<ISupermarket> _supermarkets;
        private IDirectory directory;

        public RepositoryDAO(string datastoreName)
        {
            Assembly datastoreAssembly = Assembly.UnsafeLoadFrom(datastoreName);
            Type directoryType = datastoreAssembly.GetType("Datastore.Directory");
            directory = (IDirectory) Activator.CreateInstance(directoryType);

            _products = directory.GetAllProducts();
            _supermarkets = directory.GetAllSupermarkets();
        }

        public IProduct CreateProduct()
        {
            return directory.CreateProduct();
        }

        public void RemoveProduct(IProduct product)
        {
            directory.RemoveProduct(product);
        }
    }
}
