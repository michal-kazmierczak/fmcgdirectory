﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Datastore
{
    class Product: IProduct
    {
        public Guid ID { get; }
        public string Name { get; set; }
        public Category? Category { get; set; }
        public ISupermarket Supermarket { get; set; }
        public double PriceRegular { get; set; }
        public double PriceDiscount { get; set; }
        public int DiscountAsPercentage
        {
            get
            {
                int discount = (int)((1 - (PriceDiscount / PriceRegular)) * 100);
               
                return discount < 0 ? 0 : discount;
            }
        }

        public Product(
            string name,
            Category category,
            ISupermarket supermarket,
            double priceDiscount,
            double priceRegular
        )
        {
            ID = Guid.NewGuid();
            Name = name;
            Category = category;
            Supermarket = supermarket;
            PriceRegular = priceRegular;
            PriceDiscount = priceDiscount;
        }

        public Product()
        {
            ID = Guid.NewGuid();
        }
    }
}
