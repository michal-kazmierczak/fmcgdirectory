﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Datastore
{
    class Supermarket: ISupermarket
    {
        public Guid ID { get; }
        public string Name { get; set; }
        public int FoundationYear { get; set; }

        public Supermarket(string name, int foundationYear)
        {
            ID = Guid.NewGuid();
            Name = name;
            FoundationYear = foundationYear;
        }
    }
}
