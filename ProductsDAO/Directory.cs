﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Datastore
{
    public class Directory: IDirectory
    {
        private List<ISupermarket> supermarkets;
        private List<IProduct> products;

        public Directory()
        {
            seedInitialData();
        }
        public List<IProduct> GetAllProducts()
        {
            return products;
        }
        public List<ISupermarket> GetAllSupermarkets()
        {
            return supermarkets;
        }

        public IProduct CreateProduct()
        {
            return new Product();
        }

        public void RemoveProduct(IProduct product)
        {
            products.Remove(product);
        }

        private void seedInitialData()
        {
            // supermarkets
            supermarkets = new List<ISupermarket>();
            Supermarket sampleSupermarket1 = new Supermarket("Biedronka", 1995);
            supermarkets.Add(sampleSupermarket1);

            Supermarket sampleSupermarket2 = new Supermarket("Lidl", 1930);
            supermarkets.Add(sampleSupermarket2);

            Supermarket sampleSupermarket3 = new Supermarket("Netto", 1981);
            supermarkets.Add(sampleSupermarket3);

            Supermarket sampleSupermarket4 = new Supermarket("Piotr i Paweł", 1990);
            supermarkets.Add(sampleSupermarket4);

            // products
            products = new List<IProduct>();

            products.Add(new Product("Pomidory", Category.Warzywa, sampleSupermarket1, 3.99, 6.99));
            products.Add(new Product("Jabłka", Category.Owoce, sampleSupermarket1, 2.99, 3.99));
            products.Add(new Product("Maseczka higieniczna", Category.Chemia, sampleSupermarket1, 99.99, 0.99));
            products.Add(new Product("Bułka wysokobiałkowa", Category.Pieczywo, sampleSupermarket1, 1.09, 1.39));
            products.Add(new Product("Sok pomarańczowy", Category.Napoje, sampleSupermarket1, 4.99, 4.99));

            products.Add(new Product("Cukinia", Category.Warzywa, sampleSupermarket2, 3.99, 7.99));
            products.Add(new Product("Czosnek", Category.Warzywa, sampleSupermarket2, 2.99, 3.49));
            products.Add(new Product("Sidolux", Category.Chemia, sampleSupermarket2, 9.49, 9.49));
            products.Add(new Product("Ręcznik Velvet", Category.Owoce, sampleSupermarket2, 2.99, 3.99));
            products.Add(new Product("Woda Saguaro", Category.Napoje, sampleSupermarket2, 1.19, 1.49));

            products.Add(new Product("Mięso mielone", Category.Nabiał, sampleSupermarket3, 3.99, 5.99));
            products.Add(new Product("Cytryny", Category.Owoce, sampleSupermarket3, 3.99, 5.99));
            products.Add(new Product("Mozarella", Category.Nabiał, sampleSupermarket3, 1.49, 2.19));
            products.Add(new Product("Ice Team", Category.Napoje, sampleSupermarket3, 2.49, 2.99));
            products.Add(new Product("Masło", Category.Owoce, sampleSupermarket3, 3.99, 4.99));

            products.Add(new Product("Łopatka wieprzowa", Category.Nabiał, sampleSupermarket4, 10.99, 15.99));
            products.Add(new Product("Ser żółty", Category.Nabiał, sampleSupermarket4, 4.49, 5.09));
            products.Add(new Product("Pomidor śliwkowy", Category.Warzywa, sampleSupermarket4, 2.99, 4.99));
            products.Add(new Product("Żeberka wieprzowe", Category.Nabiał, sampleSupermarket4, 10.99, 13.99));
            products.Add(new Product("Truskawki opak.", Category.Owoce, sampleSupermarket4, 4.99, 8.99));
        }


    }
}
