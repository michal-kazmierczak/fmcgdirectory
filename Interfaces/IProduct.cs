﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IProduct
    {
        Guid ID { get; }
        string Name { get; set; }
        Category? Category { get; set; }
        ISupermarket Supermarket { get; set; }
        double PriceDiscount { get; set; }
        double PriceRegular { get; set; }
        int DiscountAsPercentage { get; }
    }
}
