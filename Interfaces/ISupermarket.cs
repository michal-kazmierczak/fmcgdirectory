﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ISupermarket
    {
        Guid ID { get; }
        string Name { get; set; }
        int FoundationYear { get; set; } // pretty irrelevant to the domain problem
    }
}
