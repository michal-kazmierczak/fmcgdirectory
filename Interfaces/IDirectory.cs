﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IDirectory
    {
        List<IProduct> GetAllProducts();
        List<ISupermarket> GetAllSupermarkets();
        IProduct CreateProduct();
        void RemoveProduct(IProduct product);
    }
}
